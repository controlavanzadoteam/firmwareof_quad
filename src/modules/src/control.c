/*
 * control.c
 *
 *  Created on: Sep 9, 2016
 *      Author: oscar
 */


#include <math.h>

#include "FreeRTOS.h"
#include "task.h"

#include "system.h"
#include "log.h"
#include "param.h"

#include "control.h"
#include "lqr.h"

#include "sensors.h"
#include "commander.h"
#include "sitaw.h"
#include "controller.h"
#include "power_distribution.h"
#include "stabilizer_types.h"
#include "motors.h"


#ifdef ESTIMATOR_TYPE_kalman
#include "estimator_kalman.h"
#else
#include "estimator.h"
#endif

#include <led.h>
#include "led.h"


static bool isInit=false;

// State variables for the stabilizer
static setpoint_t setpoint;
static sensorData_t sensorData;
static state_t state;
static control_t control;


static void controlTask(void* param);


void controlInit(void)
{
	if(isInit)
		return;

	sensorsInit();
	stateEstimatorInit();
	powerDistributionInit();
	xTaskCreate(controlTask, CONTROL_TASK_NAME,
			CONTROL_TASK_STACKSIZE, NULL, CONTROL_TASK_PRI, NULL);

	isInit = true;

}

bool controlTest(void)
{
	bool pass = true;

	pass &= sensorsTest();
	pass &= stateEstimatorTest();
	pass &= lqrTest();
	pass &= powerDistributionTest();

	return pass;
}

static void controlTask(void* param)
{
	uint32_t tick = 0;
	uint32_t lastWakeTime;
	vTaskSetApplicationTaskTag(0, (void*)TASK_CONTROL_ID_NBR);

	//Wait for the system to be fully started to start stabilization loop
	systemWaitStart();

	// Wait for sensors to be calibrated
	lastWakeTime = xTaskGetTickCount ();
	while(!sensorsAreCalibrated()) {
		vTaskDelayUntil(&lastWakeTime, F2T(RATE_MAIN_LOOP));
	}

	while(1) {
		vTaskDelayUntil(&lastWakeTime, F2T(RATE_MAIN_LOOP));
		GPIOControlSet(0);

		#ifdef ESTIMATOR_TYPE_kalman
		stateEstimatorUpdate(&state, &sensorData, &control);
		#else
		sensorsAcquire(&sensorData, tick);
		stateEstimator(&state, &sensorData, tick);
		#endif

		commanderGetSetpoint(&setpoint, &state);

		sitAwUpdateSetpoint(&setpoint, &sensorData, &state);

		vlqr(&setpoint,&sensorData,&state);/*lqr function*/

		vlqr(&setpoint,&sensorData,&state);/*lqr function*/
		GPIOControlSet(1);

		tick++;
	}
}


LOG_GROUP_START(ctrltarget)
LOG_ADD(LOG_FLOAT, roll, &setpoint.attitude.roll)
LOG_ADD(LOG_FLOAT, pitch, &setpoint.attitude.pitch)
LOG_ADD(LOG_FLOAT, yaw, &setpoint.attitudeRate.yaw)
LOG_GROUP_STOP(ctrltarget)

LOG_GROUP_START(lqr)
LOG_ADD(LOG_FLOAT, roll, &state.attitude.roll)/*type data name of datam*/
LOG_ADD(LOG_FLOAT, pitch, &state.attitude.pitch)
LOG_ADD(LOG_FLOAT, yaw, &state.attitude.yaw)
LOG_ADD(LOG_UINT16, thrust, &control.thrust)
LOG_GROUP_STOP(lqr)

LOG_GROUP_START(acc)
LOG_ADD(LOG_FLOAT, x, &sensorData.acc.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.acc.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.acc.z)
LOG_GROUP_STOP(acc)

LOG_GROUP_START(baro)
LOG_ADD(LOG_FLOAT, asl, &sensorData.baro.asl)
LOG_ADD(LOG_FLOAT, temp, &sensorData.baro.temperature)
LOG_ADD(LOG_FLOAT, pressure, &sensorData.baro.pressure)
LOG_GROUP_STOP(baro)

LOG_GROUP_START(gyro)
LOG_ADD(LOG_FLOAT, x, &sensorData.gyro.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.gyro.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.gyro.z)
LOG_GROUP_STOP(gyro)

LOG_GROUP_START(mag)
LOG_ADD(LOG_FLOAT, x, &sensorData.mag.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.mag.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.mag.z)
LOG_GROUP_STOP(mag)

LOG_GROUP_START(controller)
LOG_ADD(LOG_INT16, ctr_yaw, &control.yaw)
LOG_ADD(LOG_INT16, ctr_pitvh, &control.pitch)
LOG_ADD(LOG_INT16, ctr_roll, &control.roll)
LOG_ADD(LOG_FLOAT, ctr_thrust, &control.thrust)
LOG_GROUP_STOP(controller)


