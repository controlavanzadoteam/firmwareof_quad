/*
 * lqr.c
 *
 *  Created on: Sep 9, 2016
 *      Author: oscar
 */
#include <math.h>
#include "lqr.h"
#include "num.h"
#include "motors.h"
#include "log.h"


static struct {
	uint16_t m1;
	uint16_t m2;
	uint16_t m3;
	uint16_t m4;
} motorPower;

#define M1 motorPower.m1
#define M2 motorPower.m2
#define M3 motorPower.m3
#define M4 motorPower.m4

#define limitThrust(VAL) limitUint16(VAL)


#define COSNTANTUT 	(float)38000
#define CONSTANTUX  	(float)16000000
#define CONSTANTUY  	(float)16000000
#define CONSTANTUZ  	(float)1000000
#define CONSTANTROLL  	(float)12000000
#define CONSTANTPITCH 	(float)12000000
#define CONSTANTYAW 	(float)60000000

float thrust;
float wx,wy,wz,yaw,pitch,roll;


void vlqr(const setpoint_t *setpoint,const sensorData_t *sensorData,state_t *state)
{
	float tmp1,tmp2,tmp3,tmp4;
	thrust=COSNTANTUT*setpoint->thrust;
	wx = CONSTANTUX*(sensorData->gyro.x);
	wy= CONSTANTUY*(sensorData->gyro.y);
	wz= CONSTANTUZ*(sensorData->gyro.z);
	roll=CONSTANTROLL*(state->attitude.roll-setpoint->attitude.roll);
	pitch=CONSTANTPITCH*(state->attitude.pitch-setpoint->attitude.pitch);
	yaw=CONSTANTYAW* (state->attitude.yaw-setpoint->attitudeRate.yaw);

	tmp1= thrust+wx+wy+wz+roll-pitch+yaw;
	tmp2= thrust+wx-wy-wz+roll+pitch-yaw;
	tmp3= thrust-wx-wy+wz-roll+pitch+yaw;
	tmp4= thrust-wx+wy-wz-roll-pitch-yaw;



	if (tmp1>0)
	{
		tmp1=sqrt(tmp1);
		M1=limitThrust(tmp1);
	}
	else
	{
		M1=0;
	}

	if (tmp2>0)
	{
		tmp2=sqrt(tmp2);
		M2=limitThrust(tmp2);

	}
	else
	{
		M2=0;
	}
	if (tmp3>0)
	{
		tmp3=sqrt(tmp3);
		M3=limitThrust(tmp3);

	}
	else
	{
		M3=0;
	}
	if (tmp4>0)
	{
		tmp4=sqrt(tmp4);
		M4=limitThrust(tmp4);

	}
	else
	{
		M4=0;
	}



	motorsSetRatio(MOTOR_M1,M1);
	motorsSetRatio(MOTOR_M2,M2);
	motorsSetRatio(MOTOR_M3,M3);
	motorsSetRatio(MOTOR_M4,M4);





}


bool lqrTest(void)
{
	return true;
}


LOG_GROUP_START(motor)
LOG_ADD(LOG_UINT16, motor1, &M1)
LOG_ADD(LOG_UINT16, motor2, &M1)
LOG_ADD(LOG_UINT16, motor3, &M1)
LOG_ADD(LOG_UINT16, motor4, &M1)
LOG_GROUP_STOP(motor)

LOG_GROUP_START(ANGLES)
LOG_ADD(LOG_FLOAT,wx,&wx)
LOG_ADD(LOG_FLOAT,wy,&wy)
LOG_ADD(LOG_FLOAT,wz,&wz)
LOG_GROUP_STOP(ANGLES)

LOG_GROUP_START(lqr)
LOG_ADD(LOG_FLOAT,roll,&roll)
LOG_ADD(LOG_FLOAT,yaw,&yaw)
LOG_ADD(LOG_FLOAT,pitch,&pitch)
LOG_GROUP_STOP(lqr)




