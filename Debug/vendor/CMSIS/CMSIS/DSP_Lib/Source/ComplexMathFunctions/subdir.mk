################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.c 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_conj_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_dot_prod_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mag_squared_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_cmplx_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/arm_cmplx_mult_real_q31.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/ComplexMathFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


