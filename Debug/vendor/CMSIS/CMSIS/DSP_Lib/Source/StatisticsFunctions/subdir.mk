################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q31.c 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q31.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_max_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_mean_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_min_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_power_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_rms_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_std_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/arm_var_q31.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/StatisticsFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


