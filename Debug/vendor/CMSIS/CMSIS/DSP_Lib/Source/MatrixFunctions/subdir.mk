################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.c 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_add_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_cmplx_mult_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_init_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_inverse_f64.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_fast_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_mult_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_scale_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_sub_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/arm_mat_trans_q31.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/MatrixFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


