################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/HAL_CM.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_CMSIS.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Event.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_List.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mailbox.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_MemBox.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Memory.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mutex.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Robin.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Semaphore.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_System.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Task.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Time.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Timer.c 

OBJS += \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/HAL_CM.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_CMSIS.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Event.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_List.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mailbox.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_MemBox.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Memory.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mutex.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Robin.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Semaphore.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_System.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Task.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Time.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Timer.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/HAL_CM.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_CMSIS.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Event.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_List.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mailbox.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_MemBox.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Memory.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Mutex.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Robin.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Semaphore.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_System.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Task.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Time.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/SRC/rt_Timer.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/RTOS/RTX/SRC/%.o: ../vendor/CMSIS/CMSIS/RTOS/RTX/SRC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


