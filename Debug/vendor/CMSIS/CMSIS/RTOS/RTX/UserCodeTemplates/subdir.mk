################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.c \
../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/main.c 

OBJS += \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.o \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/main.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MailQueue.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MemPool.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/MsgQueue.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Mutex.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Semaphore.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Thread.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/Timer.d \
./vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/main.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/%.o: ../vendor/CMSIS/CMSIS/RTOS/RTX/UserCodeTemplates/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


