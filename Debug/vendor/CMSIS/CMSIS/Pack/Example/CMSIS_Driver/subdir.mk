################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.c \
../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.c 

OBJS += \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.o \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/EMAC_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPDMA_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/GPIO_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2C_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/I2S_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/MCI_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SCU_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/SSP_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USART_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB0_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USB1_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD0_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBD1_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH0_LPC18xx.d \
./vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/USBH1_LPC18xx.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/%.o: ../vendor/CMSIS/CMSIS/Pack/Example/CMSIS_Driver/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


