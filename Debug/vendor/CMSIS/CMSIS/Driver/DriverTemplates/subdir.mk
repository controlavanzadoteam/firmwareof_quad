################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_CAN.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_Flash.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_I2C.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_MCI.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SAI.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SPI.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USART.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBD.c \
../vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBH.c 

OBJS += \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_CAN.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_Flash.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_I2C.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_MCI.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SAI.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SPI.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USART.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBD.o \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBH.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_CAN.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_MAC.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_ETH_PHY.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_Flash.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_I2C.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_MCI.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SAI.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_SPI.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USART.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBD.d \
./vendor/CMSIS/CMSIS/Driver/DriverTemplates/Driver_USBH.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/Driver/DriverTemplates/%.o: ../vendor/CMSIS/CMSIS/Driver/DriverTemplates/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


