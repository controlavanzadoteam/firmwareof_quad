################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP.c \
../vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP_vendor.c \
../vendor/CMSIS/CMSIS/DAP/Firmware/Source/JTAG_DP.c \
../vendor/CMSIS/CMSIS/DAP/Firmware/Source/SWO.c \
../vendor/CMSIS/CMSIS/DAP/Firmware/Source/SW_DP.c 

OBJS += \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP.o \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP_vendor.o \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/JTAG_DP.o \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/SWO.o \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/SW_DP.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP.d \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/DAP_vendor.d \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/JTAG_DP.d \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/SWO.d \
./vendor/CMSIS/CMSIS/DAP/Firmware/Source/SW_DP.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DAP/Firmware/Source/%.o: ../vendor/CMSIS/CMSIS/DAP/Firmware/Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


