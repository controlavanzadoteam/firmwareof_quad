################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/startup_ARMSC000.c 

S_UPPER_SRCS += \
../vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/startup_ARMSC000.S 

OBJS += \
./vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/startup_ARMSC000.o 

C_DEPS += \
./vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/startup_ARMSC000.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/%.o: ../vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/%.o: ../vendor/CMSIS/Device/ARM/ARMSC000/Source/GCC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


