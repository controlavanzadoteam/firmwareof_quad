################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/startup_ARMCM0plus.c 

S_UPPER_SRCS += \
../vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/startup_ARMCM0plus.S 

OBJS += \
./vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/startup_ARMCM0plus.o 

C_DEPS += \
./vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/startup_ARMCM0plus.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/%.o: ../vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/%.o: ../vendor/CMSIS/Device/ARM/ARMCM0plus/Source/GCC/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


