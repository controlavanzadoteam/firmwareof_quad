################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../bin/attitude_pid_controller.o \
../bin/buzzer.o \
../bin/comm.o \
../bin/commander.o \
../bin/console.o \
../bin/controller_pid.o \
../bin/crtp.o \
../bin/crtpservice.o \
../bin/estimator_complementary.o \
../bin/exti.o \
../bin/freeRTOSdebug.o \
../bin/heap_4.o \
../bin/ledseq.o \
../bin/list.o \
../bin/log.o \
../bin/main.o \
../bin/mem.o \
../bin/motors.o \
../bin/nvic.o \
../bin/param.o \
../bin/pid.o \
../bin/port.o \
../bin/position_controller_pid.o \
../bin/position_estimator_altitude.o \
../bin/power_distribution_stock.o \
../bin/queue.o \
../bin/queuemonitor.o \
../bin/sensfusion6.o \
../bin/sensors_stock.o \
../bin/sitaw.o \
../bin/stabilizer.o \
../bin/stm32f4xx_adc.o \
../bin/stm32f4xx_dbgmcu.o \
../bin/stm32f4xx_dma.o \
../bin/stm32f4xx_exti.o \
../bin/stm32f4xx_flash.o \
../bin/stm32f4xx_gpio.o \
../bin/stm32f4xx_i2c.o \
../bin/stm32f4xx_iwdg.o \
../bin/stm32f4xx_misc.o \
../bin/stm32f4xx_rcc.o \
../bin/stm32f4xx_spi.o \
../bin/stm32f4xx_syscfg.o \
../bin/stm32f4xx_tim.o \
../bin/stm32f4xx_usart.o \
../bin/system.o \
../bin/tasks.o \
../bin/timers.o \
../bin/trigger.o \
../bin/worker.o 


# Each subdirectory must supply rules for building sources it contributes

