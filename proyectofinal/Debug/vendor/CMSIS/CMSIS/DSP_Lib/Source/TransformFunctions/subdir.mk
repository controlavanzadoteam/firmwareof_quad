################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.c 

S_UPPER_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.S 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal2.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_bitreversal.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_init_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix2_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_init_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix4_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_cfft_radix8_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_init_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_dct4_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_fast_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_init_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/arm_rfft_q31.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/TransformFunctions/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


