################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.c 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_cos_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sin_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/arm_sqrt_q31.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/FastMathFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


