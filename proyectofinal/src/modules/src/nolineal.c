#include "nolineal.h"
#include "Differentiator.h"
#include <math.h>
#include "num.h"
#include "motors.h"
#include "log.h"
#include "stabilizer_types.h"
#include "stabilizer.h"
#include "estimator_kalman.h"

#define N_DER (3)//definir derivadas
#define PI (3.141591f)

static struct {
	uint16_t m1;
	uint16_t m2;
	uint16_t m3;
	uint16_t m4;
} motorPower;

#define M1 motorPower.m1
#define M2 motorPower.m2
#define M3 motorPower.m3
#define M4 motorPower.m4
#define REF_PITCH 		setpoint->attitude.pitch
#define REF_ROLL		setpoint->attitude.roll
#define REF_YAW			setpoint->attitude.yaw
#define REF_THRUST		setpoint->thrust
#define CONV (PI/180.0f)
#define limitThrust(VAL) (limitUint16(VAL))
static Diff_t diff_roll;
static Diff_t diff_pitch;
static Diff_t diff_yaw;
#define REF_ROLL_1DER   diff_roll.z_state[1]
#define REF_ROLL_2DER   diff_roll.z_state[2]
#define REF_YAW_1DER   	diff_yaw.z_state[1]
#define REF_YAW_2DER 	diff_yaw.z_state[2]
#define REF_PITCH_1DER  diff_pitch.z_state[1]
#define REF_PITCH_2DER  diff_pitch.z_state[2]
#define WX 				((sensorData->gyro.x)*CONV)
#define WY				((sensorData->gyro.y)*CONV)
#define WZ				((sensorData->gyro.z)*CONV)
#define YAW 			((state->attitude.yaw)*CONV)
#define ROLL 			((state->attitude.roll)*CONV)//(float) 0.00001
#define PITCH 			((state->attitude.pitch)*CONV)//(float) 0.00001
//ROLL
static float k_g_R [N_DER+1] ={6, 14, 14 , 5};
static float rho_R = 1;
//PITCH
static float k_g_P [N_DER+1] ={6, 14, 14 , 5};
static float rho_P = 1;
//YAW
static float k_g_Y [N_DER+1] ={6, 14, 14 , 5};
static float rho_Y = 1;
const  float dt = 0.001;
//Declaracion de valores constantes
/*constantes
//Ct = 215e-3       
//Cq = 7.1e-3       
//d = 0.0325        */
const float Ix= 0.000023951;//2.3951e-5;     
const float Iy= 0.000023951;//2.3951e-5;     
const float Iz = 0.000032347; 
void vNonLinealControl_init(void)
{
	D_init (&diff_roll, k_g_R, rho_R, dt, N_DER);
	D_init (&diff_pitch, k_g_P, rho_P, dt, N_DER);
	D_init (&diff_yaw, k_g_Y, rho_Y, dt, N_DER);
}
void vNonLinealControl(const setpoint_t *setpoint,const sensorData_t *sensorData,state_t *state)
{	
	float tmp1,tmp2,tmp3,tmp4;//valores de las u's
	float YAWD,PITCHD,ROLLD,V1,V2,V3,V4,U1,U2,U3,U4;
	float A,B,C,D;/*temporales para ecuaciones de motores*/
	/***CONTROL WAS HERE**/
	//obtener referencias OSCAR datos del comander DONE/*DEFINES*/
	//derivar referencias
	D_refresh (&diff_roll,REF_ROLL);
	D_refresh (&diff_yaw,REF_YAW);
	D_refresh (&diff_pitch,REF_PITCH);


	//Ecuaciones necesarias para que funcione el derivador obtenidas del modelo del Quad  

	A=cos(ROLL);
	B=sin(ROLL);
	C=cos(PITCH);
	D=sin(PITCH);
	YAWD = ((B/C) * WY) + ((A/C) * WZ);
	PITCHD = (A * WY) - (B* WZ);
	ROLLD = WX + (D/C)*(B*WY) + (D/C)* A * WZ;


	// Ecuaciones de V_s para el control  
	V1 = 1;
	V2 = REF_ROLL_2DER - 2000 * (ROLL - REF_ROLL) - 500 *(ROLLD-REF_ROLL_1DER);
	V3 = (-1.0f)*(REF_PITCH_2DER - 1000 * (PITCH - REF_PITCH) - 55 *(PITCHD-REF_PITCH_1DER));
	V4 = REF_YAW_2DER - 2000 * (YAW - REF_YAW) - 2000 *(YAWD-REF_YAW_1DER);

	//Ecuaciones de control
	tmp1=cos(2* ROLL);
	U1 = REF_THRUST;
	U2 = -Iy* WY* WZ + Iz* WY* WZ - Ix* WY* WZ* tmp1 - Ix* WY*WY* A* B + Ix* WZ*WZ* A* B + Ix* V2 - Ix* D* V4;
	U3 =  Ix* WX* WZ* A*A + Iy* WX* WZ* A*A - Iz* WX* WZ* A*A + Ix* WX* WZ* B*B + Iy* WX *WZ* B*B - Iz* WX* WZ* B*B + Iy* A* V3 + Iy *C* B* V4 + Iy* WZ*WZ* A*A*A* (D/C) - Iy* WY*WY* A* B*B* (D/C) + 2* Iy* WZ*WZ* A* B*B* (D/C) + 2* Iy *WY* WZ* B*B*B* (D/C);
	U4 = -Ix* WX* WY *A*A + Iy* WX* WY *A*A - Iz* WX *WY* A*A - Ix* WX* WY* B*B + Iy* WX* WY* B*B - Iz* WX* WY* B*B - Iz* B* V3 + Iz* C* A* V4 - 2* Iz* WY* WZ *A*A*A* (D/C) -2 *Iz* WY*WY* A*A* B* (D/C) + Iz* WZ*WZ* A*A* B* (D/C) - Iz* WY*WY* B*B*B* (D/C);

	//Ecuaciones para implementar el control en los motores

	A= 1.16* U1;
	B= 50.6 * U2;
	C= 50.6 * U3;
	D= 35.21 * U4;
	tmp1 = A - B - C - D;//w²
	tmp2 = A - B + C + D;//w²
	tmp3 = A + B + C - D;//w²
	tmp4 = A + B - C + D;//w²


	/*************************************************/


	/*las temp son las u*/
	//asignacion de potencia a motores
	if (tmp1>0)
	{
		tmp1=sqrt(tmp1)*(UINT16_MAX);
		M1=limitThrust(tmp1);
	}
	else
	{
		M1=0;
	}

	if (tmp2>0)
	{
		tmp2=sqrt(tmp2)*(UINT16_MAX);
		M2=limitThrust(tmp2);

	}
	else
	{
		M2=0;
	}
	if (tmp3>0)
	{
		tmp3=sqrt(tmp3)*(UINT16_MAX);
		M3=limitThrust(tmp3);

	}
	else
	{
		M3=0;
	}
	if (tmp4>0)
	{
		tmp4=sqrt(tmp4)*(UINT16_MAX);
		M4=limitThrust(tmp4);

	}
	else
	{
		M4=0;
	}



	motorsSetRatio(MOTOR_M1,M1);
	motorsSetRatio(MOTOR_M2,M2);
	motorsSetRatio(MOTOR_M3,M3);
	motorsSetRatio(MOTOR_M4,M4);

}

bool vNonLinealTest(void)
{
	return true;
}

	