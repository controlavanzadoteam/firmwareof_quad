/*
 * lqr.h
 *
 *  Created on: Sep 9, 2016
 *      Author: oscar
 */

#ifndef SRC_MODULES_INTERFACE_LQR_H_
#define SRC_MODULES_INTERFACE_LQR_H_

#include <stdbool.h>
#include <stdint.h>
#include "stabilizer_types.h"

bool lqrTest(void);
void vlqr(const setpoint_t *setpoint,const sensorData_t *sensorData,state_t *state);


#endif /* SRC_MODULES_INTERFACE_LQR_H_ */
