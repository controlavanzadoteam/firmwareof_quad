################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q7.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_f32.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q15.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q31.c \
../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q7.c 

OBJS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q7.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_f32.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q15.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q31.o \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q7.o 

C_DEPS += \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_abs_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_add_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_dot_prod_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_mult_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_negate_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_offset_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_scale_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_shift_q7.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_f32.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q15.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q31.d \
./vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/arm_sub_q7.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/%.o: ../vendor/CMSIS/CMSIS/DSP_Lib/Source/BasicMathFunctions/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


