################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../vendor/CMSIS/Device/_Template_Flash/Test/FlashDev.c \
../vendor/CMSIS/Device/_Template_Flash/Test/FlashPrg.c \
../vendor/CMSIS/Device/_Template_Flash/Test/FlashTest.c \
../vendor/CMSIS/Device/_Template_Flash/Test/system_stm32f10x.c 

OBJS += \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashDev.o \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashPrg.o \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashTest.o \
./vendor/CMSIS/Device/_Template_Flash/Test/system_stm32f10x.o 

C_DEPS += \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashDev.d \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashPrg.d \
./vendor/CMSIS/Device/_Template_Flash/Test/FlashTest.d \
./vendor/CMSIS/Device/_Template_Flash/Test/system_stm32f10x.d 


# Each subdirectory must supply rules for building sources it contributes
vendor/CMSIS/Device/_Template_Flash/Test/%.o: ../vendor/CMSIS/Device/_Template_Flash/Test/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


