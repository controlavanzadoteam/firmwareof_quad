/*
 * derivarod.h
 *
 *  Created on: Nov 20, 2016
 *      Author: oscar
 */

#ifndef DIFFERENTIATOR_H_
#define DIFFERENTIATOR_H_

#define N_MAX (10)

typedef unsigned char uint8_t;

typedef struct stDiff_t
{

	float K_gains [N_MAX];
	float rho;
	float z_state [N_MAX];
	float dt;
	uint8_t N_der;

}Diff_t;

void D_init (Diff_t *D, float *k_g, float rho, float dt, uint8_t N_der);
void D_refresh (Diff_t *D, float func);



#endif /* DERIVAROD_H_ */
