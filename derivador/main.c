/*
 * main.c
 *
 *  Created on: Nov 20, 2016
 *      Author: oscar
 */

#include<stdio.h>
#include<stdlib.h>
#include <math.h>

#include "Differentiator.h"
#define N_DER (2)

u_int8_t const SINE_WAVE[256] = {
		128,131,134,137,140,144,147,150,153,156,159,162,165,168,171,174,
		177,179,182,185,187,191,193,196,199,201,204,206,209,211,213,216,
		218,220,222,224,226,228,230,232,233,235,237,239,240,241,243,244,
		245,246,248,249,250,250,251,252,253,253,254,254,254,255,255,255,
		255,255,255,255,254,254,254,253,253,252,251,250,250,249,248,246,
		245,244,243,241,240,239,237,235,234,232,230,228,226,224,222,220,
		218,216,213,211,209,206,203,201,199,196,193,191,188,185,182,179,
		177,174,171,168,165,162,159,156,153,150,147,144,140,137,134,131,
		128,125,122,119,116,112,109,106,103,100,97,94,91,88,85,82,79,76,
		74,71,68,65,63,60,57,54,52,49,47,45,43,40,38,36,34,32,30,28,26,
		24,22,21,19,17,16,15,13,12,11,10,8,7,6,6,5,4,3,3,2,1,1,1,1,1,1,
		1,1,1,2,2,2,3,3,4,5,6,6,7,8,10,11,12,13,15,16,17,19,21,24,26,28,
		30,32,34,36,38,40,43,45,47,50,52,55,57,60,63,65,68,71,74,77,79,
		82,85,88,91,94,97,100,103,106,109,112,116,119,122,125,128};



int main()
{
	FILE *fp;
	Diff_t diff_roll;
	Diff_t diff_pitch;
	Diff_t diff_yaw;
	float k_g [N_DER+1] ={1.2, 8.5, 6.1};
	float rho = 0.9;
	float dt = 0.001;
	float roll_ref,coseno;
	u_int32_t count=0;
	D_init (&diff_roll, k_g, rho, dt, N_DER);
	D_init (&diff_pitch, k_g, rho, dt, N_DER);
	D_init (&diff_yaw, k_g, rho, dt, N_DER);
	u_int32_t limit =50000;

	while(count <= limit)
	{
		//roll_ref=SINE_WAVE[count];
		roll_ref=(float)(sin(count*(dt)));
		coseno=(float)(cos(count*(dt)));
		D_refresh (&diff_roll,roll_ref);
		diff_roll.z_state[0]; //ref
		diff_roll.z_state[1]; //1 derivada ref
		diff_roll.z_state[2]; //segunda derivada ref
		printf("%f",roll_ref);
		printf("\t");
		printf("%f",diff_roll.z_state[0]);
		printf("\t");
		printf("%f",diff_roll.z_state[1]);
		printf("\t");
		printf("%f",diff_roll.z_state[2]);
		printf("\r\n");
		count++;
	}

	count=0;
}
