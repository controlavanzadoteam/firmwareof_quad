/*
 * derivador.c


 *
 *  Created on: Nov 20, 2016
 *      Author: oscar
 */

#include "Differentiator.h"
#include <math.h>


float c_sign (float x);
float c_abs(float x);


void D_init (Diff_t *D, float *k_g, float rho, float dt, uint8_t N_der)
{

	uint8_t index;
	D->dt = dt;
	D->N_der = N_der;
	D->rho = rho;

	for ( index=0 ; index < N_der+1 ; index++)
	{
		D->K_gains[index] = k_g[index];
		D->z_state[index] = 0;
	}

}

void D_refresh(Diff_t *D, float func)
{

	uint8_t index;
	float sigma = func - D->z_state[0];

	for (index = 0; index < D->N_der; index++)
	{
		D->z_state[index] = (D->z_state[index]) \
							+ (D->dt) \
							* ( (D->z_state[index+1]) \
							+ pow(D->rho,(index+1) )  \
							* D->K_gains[index] \
							* pow(c_abs(sigma),\
							  ((float)  ( (float) (D->N_der) - (float) index ) / (float)(D->N_der+1.0f) ) ) \
							* c_sign(sigma) );
	}

	D->z_state[D->N_der] = 	  D->z_state[D->N_der] \
							+ D->dt\
							* (pow(D->rho,((D->N_der)+1) )\
							* D->K_gains[D->N_der]\
							* c_sign(sigma) );

}

float c_sign(float x)
{
	return x> 0.0 ? 1.0 : -1.0;
}

float c_abs (float x)
{
	return x > 0 ? x : -x;
}


